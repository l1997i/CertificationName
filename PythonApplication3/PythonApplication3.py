#coding=utf-8


# 证书自动交换组员姓名
# Dec. 7th, 2018 - Ver1.0A
# 编译环境：Python 3.6 + Microsoft Visual Studio Enterprise 2017
# 电光学院科学与技术协会 李理

# 注意事项：
# 选手姓名列一定要放在最后一列
# 选手之间只能使用空格分隔，否则将无法识别

import xlrd
import xlwt

readbook = xlrd.open_workbook(r'a.xlsx') # 待处理奖状信息文件路径，绝对路径
sheet = readbook.sheet_by_name('Sheet1')
f = xlwt.Workbook()
sheet1 = f.add_sheet('info',cell_overwrite_ok=True)

isTitle = 1               # 1：第一行是标题   0：第一行不是标题
nameCol = sheet.ncols     # 选手姓名列一定要放在最后一列
nrows = sheet.nrows     # 行数
ncols = sheet.ncols     # 列数

rTitle=[0]*ncols
rVal=[]
rowVal=[[]]
index = 0
currentRow = 0
for r in range(isTitle,nrows):
    rVal=[]
    rowVal=[[]]
    index = 0
    if isTitle:
        for titleCol in range(ncols):
            rTitle[titleCol] = sheet.cell(0,titleCol).value
    myName = sheet.cell(r,nameCol-1).value.split()
    for i in range(len(myName)):
        rVal.append(myName[i])
        for j in range(len(myName)):
            if i!=j:
                rVal[index] = rVal[index]+" "+myName[j]
        for colVal in range(ncols):
            if colVal!=nameCol-1:
                rowVal[index].append(sheet.cell(r,colVal).value)
        if i<len(myName)-1:
            rowVal.append([])
        index = index+1
    row0 = rTitle
    colum0 = rVal
    #写数据列
    for k in range(0,len(colum0)):
        sheet1.write(currentRow+1,nameCol-1,colum0[k])
        inRow = 0
        for m in range(0,ncols-1):
            sheet1.write(currentRow+1,m,rowVal[inRow][m])
            if inRow<len(rowVal)-1:
                inRow = inRow+1
        currentRow = currentRow+1
    f.save('info.xls')
    print(myName)

# 重写标题行
row0 = rTitle
for j in range(0,len(row0)):
    sheet1.write(0,j,row0[j])
f.save('info.xls')

print("Big Success!!")