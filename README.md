[![pipeline status](http://39.96.30.77:8099/L1997i/CertificationName/badges/master/pipeline.svg)](http://39.96.30.77:8099/L1997i/CertificationName/pipelines)
[![coverage report](http://39.96.30.77:8099/L1997i/CertificationName/badges/master/coverage.svg)](http://39.96.30.77:8099/L1997i/CertificationName/commits/master)
# 证书自动交换组员姓名
Dec. 7th, 2018 - Ver1.0A

编译环境：Python 3.6 + Microsoft Visual Studio Enterprise 2017

电光学院科学与技术协会 李理

## 注意事项
- 选手姓名列一定要放在最后一列
- 选手之间只能使用空格分隔，否则将无法识别